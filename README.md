# Installation
> `npm install --save @types/mathjax`

# Summary
This package contains type definitions for MathJax (https://github.com/mathjax/MathJax).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/mathjax.

### Additional Details
 * Last updated: Thu, 08 Jul 2021 16:24:05 GMT
 * Dependencies: none
 * Global values: `MathJax`

# Credits
These definitions were written by [Roland Zwaga](https://github.com/rolandzwaga).
